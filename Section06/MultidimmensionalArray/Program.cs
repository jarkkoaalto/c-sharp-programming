﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultidimmensionalArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] arr = new int[4, 5]; // 20 numeroa
            for(int i=0; i < 4; i++)
            {
                for(int j = 0; j < 5; j++)
                {
                    arr[i, j] = Int32.Parse(Console.ReadLine()); // Read the promt
                }
            }

            for(int i=0; i<4; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.Write("\n" + arr[i, j]);
                }
            }
        }
    }
}
