﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysAndLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[10] { 3,2,6,1,8,7,6,9,11,12};
            for(int i=0; i<10; i++)
            {
                Console.WriteLine(arr[i]);
            }
        }
    }
}
