﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace String01
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = "Good day";

            Console.WriteLine(s);
            Console.WriteLine(s.Length);
            Console.WriteLine(s.IndexOf('d'));

            s = s.Replace("day", "night");
            Console.WriteLine(s);

            s = s.Insert(0, "Very ");
            Console.WriteLine(s);

        }
    }
}
