﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Method
{
    class Program
    {

        static void Printing(int x)
        {
            Console.WriteLine(x);
        }

        /**
         * Multiple parameters
         */ 
         static void Printing(int x, String name)
        {
            Console.WriteLine(x + " " + name);
        }

        static void Main(string[] args)
        {
            /**
             * Methods
             * --------
             * 
             * method name
             * optinal paraneters (parameter 1 to parameter n)
             * {
             *      list of statements
             * }
             * return type
             */

             /**
             * Paraments
             * ---------
             *  static void Printing(int x)
             * 
             */

            Printing(200);
            Printing(100, "Home");
        }
    }
}
