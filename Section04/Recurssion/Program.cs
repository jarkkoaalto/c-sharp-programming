﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recurssion
{
    class Program
    {
        static int factorial(int x)
        {
            // if given numeber is 1
            if(x == 1)
            {
                return 1;
            }
            // factorial 5-1=4,  4-1 = 3, 3-1=2, 2-1 = 1;
            // 5*4*3*2*1=120
            else
            {
                return x * factorial(x - 1);
            }
           
        }

        static void Main(string[] args)
        {
            Console.WriteLine(factorial(5));
        }
    }
}
