﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodOverloading
{
    class Program
    {

        static void abc(int a)
        {
            Console.WriteLine("value: " + a);
        }
        static void abc(double a)
        {
            Console.WriteLine("value: " + a);
        }
        static void abc(String l, int a)
        {
            Console.WriteLine("Label:" + l + "value: " + a);
        }

        static void Main(string[] args)
        {
            abc(11);
            abc(11.100) ;
            abc("Hi ", 1000);
            
        }
    }
}
