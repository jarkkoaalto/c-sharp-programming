﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThisKeyword
{
    class Program
    {
        class Abc
        {
            private string name = "abc";
            public Abc()
            {
                Console.WriteLine(this.name);
            }
        }

        static void Main(string[] args)
        {
            Abc a = new Abc();
        }
    }
}
