﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassExample
{
    class Program
    {
        class Person
        {
            int age;
            String name;

            public void wish()
            {
                Console.WriteLine("Hello, I need a car");
            }
        }

        static void Main(string[] args)
        {
            Person p1 = new Person();
            p1.wish();
            
        }
    }
}
