﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encapsulation
{
    class Program
    {
        class Bank
        {
            private double balance = 0;
            public void deposit(double x)
            {
                balance += x;
            }
            public void withdraw(double n)
            {
                balance -= n;
            }
            public double getbalance()
            {
                return balance;
            }

        }

        static void Main(string[] args)
        {
            Bank p1 = new Bank();
            p1.deposit(40.0);
            
        }
    }
}
