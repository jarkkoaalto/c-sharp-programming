﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicalOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * And
             * True       True        True
             * True       False       False
             * False      True        False
             * Flase      False       False
             * 
             * 
             * OR
             *  True       True       True
             * True       False       True
             * False      True        True
             * Flase      False       False
             * 
             * NOT
             * 
             * True                   False
             * False                  True
             */

            int age = 25;
            double money = 500;

            if(age < 30 && money < 1000)
            {
                Console.WriteLine("You are young and poor");
            }

            if(age > 18 || money > 200)
            {
                Console.WriteLine("Buy weekend bottle");
            }

        }
    }
}
