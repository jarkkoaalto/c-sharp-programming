﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NestedIf
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 22;
            int y = 24;
            if (x >= y)
            {
                if (x == y)
                {
                    Console.WriteLine("x is equal y");
                }
                
            }
            else if(x == 22)
            {
                Console.WriteLine("x == 22");
            }
        }
    }
}
