﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;

            while(num < 10)
            {
                Console.WriteLine("Num is :" + num);
                num = num + 1;
            }

        }
    }
}
