﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContinueStatement
{
    class Program
    {
        static void Main(string[] args)
        {
            

            for(int num = 10; num>0; num--)
            {
                if(num == 6)
                {
                    continue;
                }
                Console.WriteLine(num);
               
            }

            Console.WriteLine("\nNumber 6 is missing, because continue statement\n");
        }
        
    }
}
