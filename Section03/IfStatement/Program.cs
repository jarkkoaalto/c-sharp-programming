﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IfStatement
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             *  Operators
             *  =========
             *  <                   less 
             *  >                   greater
             *  >=                  greater or equal than
             *  <=                  leass than or equal than
             *  ==                  equal
             *  !                   not
             *  !=                  not equal
             * 
             */


            int x = 50;
            int y = 24;

            // If statement
            if(x > y)
            {
                Console.WriteLine("x is greater than y");
            }

            // Else Statement

            if(x < y)
            {
                Console.WriteLine("Sorry X is too large");
            }
            else
            {
                Console.WriteLine("y is the best");
            }

        }
    }
}
