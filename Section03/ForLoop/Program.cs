﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(" i = " + (i));
            }
            Console.WriteLine(" ------------ ");
            for(int j = 10; j > 0; j--)
            {
                Console.WriteLine(" j = " + j);
            }
        }
    }
}
