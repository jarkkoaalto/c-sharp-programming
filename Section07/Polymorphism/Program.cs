﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    class Program
    {

        class Shape
        {
            public virtual void draw()
            {
                Console.Write("base draw");
            } 
        }


        class Circle : Shape
        {
            public override void draw()
            {
                Console.WriteLine("Circle draw");
            }
        }


        class Rectangle : Shape
        {
            public override void draw()
            {
                Console.WriteLine("Recrangle draw");
            }

        }

        static void Main(string[] args)
        {
            Shape c = new Circle();
            c.draw();

            Shape r = new Rectangle();
            r.draw();
        }
    }
}
