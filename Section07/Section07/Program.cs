﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Section07
{
    class Program
    {

        class Person
        {
            protected int age;
            protected string name;
        }
        class Student : Person
        {
            public Student(string a)
            {
                name = a;
            }
            public void speak()
            {
                Console.WriteLine("name : " + name);
            }
        }

        static void Main(string[] args)
        {
            Student s = new Student("Kevin");
            s.speak();
        }
    }
}
